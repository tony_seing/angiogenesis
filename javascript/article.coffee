sizeUpCount = 0
articleid = {}

getParameterByName = (name)->
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]")
  regexS = "[\\?&]" + name + "=([^&#]*)"
  regex = new RegExp(regexS)
  results = regex.exec window.location.search
  if results == null
      ""
  else
      decodeURIComponent results[1].replace(/\+/g, " ")

unhighlight_words = ()->
        $('.article-content .highlighted').removeClass("highlighted")

highlight_words = (word)->
        re = new RegExp(word,"g");
        unhighlight_words()
        $('.article-content').highlightRegex( re, { \
                className: 'highlighted' 
        })


isBookmarked = ->
        # get all bookmarks
        token = {}
        token.auth_token = "79d16d053261278cb9a6837f4ff8499c6ff17725"
        $.post "http://www.cmeonline.org/api/bookmarks", JSON.stringify(token), (response)->
                test_if_mine(bookmark) for bookmark in JSON.parse(response)

hasBookmark = false                
test_if_mine = (bookmark)->
        if bookmark.articleid == getParameterByName("id")        
                hasBookmark = true
                $('.bookmark-icon').attr('src', 'images/small/btn_sm_flag_active.png') 
                console.log "this article has a bookmark"

insertBookmark = ->
  bookmark = {}
  bookmark.articleid = articleid
  bookmark.extra = "stub for now"
  bookmark.title = "test"
  bookmark.chapter = "1"
  bookmark.section = "1"
  bookmark.page = "1"
  bookmark.auth_token = "79d16d053261278cb9a6837f4ff8499c6ff17725"
  $.post "http://www.cmeonline.org/api/bookmarks/insert", JSON.stringify(bookmark), (response)->
    console.log(response)

makeFancyBox = ->
        $('.article-content img').fancybox()

scrollToAnchor = (aid)-> 
        #aTag = $("span[name='"+ aid + "']")
        #if aTag.length > 0 then $('.article-content').animate({ scrollTop: aTag.position().top  },'slow')        
        window.location.url = "##{aid}"
        

$(document).ready ->
    isBookmarked() # if this article has a bookmark, highlight
    $.post("http://www.cmeonline.org/api/articles/#{getParameterByName('id')}", JSON.stringify({ "auth_token" : "79d16d053261278cb9a6837f4ff8499c6ff17725"}), (data)-> populateArticle(JSON.parse(data)))

    populateArticle = (article)->
        if article[0].id then articleid = article[0].id
        if article[0].title then $('.article-title').text(article[0].title)
        if article[0].subtitle then $('.article-subtitle').text(article[0].subtitle)
        if article[0].content then $('.article-copy').html(article[0].content)
        makeFancyBox()

    $('.searchhighlight').keyup (e)->
        unhighlight_words()
        if e.keyCode == 13
                highlight_words($(".searchhighlight").val())        

    $('.article-btn-icon').not(".bookmark-icon").hover(
       -> $(this).attr('src', $(this).attr('active'))
       -> $(this).attr('src', $(this).attr('inactive')))

    $('.bookmark-icon').hover(
        -> $(this).attr('src', $(this).attr('active'))
        ->
                unless hasBookmark
                        $(this).attr('src', $(this).attr('inactive')))

    $('.top-back-btn').click ->
        window.location.href = "articles.html"
        $('.article-btn-icon').hover(
            -> $(this).attr('src', $(this).attr('active'))
            -> $(this).attr('src', $(this).attr('inactive')))

    $('.font-change-icon').click ->
        if sizeUpCount % 4 == 0
            $('.article-content').css('font-size', '110%')
        else if sizeUpCount % 4 == 1
            $('.article-content').css('font-size', '125%')
        else if sizeUpCount % 4 == 2
            $('.article-content').css('font-size', '160%')
        else
            $('.article-content').css('font-size', '100%')
        sizeUpCount += 1

    $('.bookmark-icon').click ->
        insertBookmark()
        false

    $('.text-search').click ->
        $('.ui-input-search').toggle()

    $('.toc-dropdown-btn').click ->
        $('.toc, .article-content').toggle()

    $('.toc-section a').click ->
        $('.toc, .article-content').toggle()
        scrollToAnchor($(this).attr("name"))
