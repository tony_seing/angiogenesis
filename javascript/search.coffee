# search function that returns the results of the term search query
getParameterByName = (name)->
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]")
    regexS = "[\\?&]" + name + "=([^&#]*)"
    regex = new RegExp(regexS)
    results = regex.exec window.location.search
    if results == null
        ""
    else
        decodeURIComponent results[1].replace(/\+/g, " ")

first_n_chars = (word, n)->
  word.slice(0, n)

search = (term)->
  $.post "http://www.cmeonline.org/api/search/#{encodeURIComponent(term)}", JSON.stringify({ "auth_token" : "79d16d053261278cb9a6837f4ff8499c6ff17725" }), (data)->
    createSearchResultsList(JSON.parse(data))

createSearchResultsList = (results)->
  createSearchResult(result) for result in results
  $('.search-list .ui-listview').append "<li data-article-id='empty' class='ui-li ui-li-static ui-btn-up-c ui-li-has-thumb ui-first-child ui-last-child'}'><h3 class='ui-li-heading'></h3><p class='ui-li-content'></li>" for num in [1..4]
  


createSearchResult  = (result)->
  $('.search-list .ui-listview').append "<li data-article-id='#{result.id}' class='ui-li ui-li-static ui-btn-up-c ui-li-has-thumb ui-first-child ui-last-child'}'><img src='#{result.image}' class='article-icon ui-li-thumb' /><h3 class='ui-li-heading'>#{result.title}</h3><p class='ui-li-content'>#{first_n_chars(result.content, 100)}...</li>"
 
  $('.search-list ul li').click ->
    window.location.href = "article.html?id=#{$(this).attr('data-article-id')}" 

$(document).ready ->
        search(getParameterByName("term"))
