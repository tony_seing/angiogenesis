// Generated by CoffeeScript 1.6.2
(function() {
  var articleid, getParameterByName, hasBookmark, highlight_words, insertBookmark, isBookmarked, makeFancyBox, scrollToAnchor, sizeUpCount, test_if_mine, unhighlight_words;

  sizeUpCount = 0;

  articleid = {};

  getParameterByName = function(name) {
    var regex, regexS, results;

    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
    regexS = "[\\?&]" + name + "=([^&#]*)";
    regex = new RegExp(regexS);
    results = regex.exec(window.location.search);
    if (results === null) {
      return "";
    } else {
      return decodeURIComponent(results[1].replace(/\+/g, " "));
    }
  };

  unhighlight_words = function() {
    return $('.article-content .highlighted').removeClass("highlighted");
  };

  highlight_words = function(word) {
    var re;

    re = new RegExp(word, "g");
    unhighlight_words();
    return $('.article-content').highlightRegex(re, {
      className: 'highlighted'
    });
  };

  isBookmarked = function() {
    var token;

    token = {};
    token.auth_token = "79d16d053261278cb9a6837f4ff8499c6ff17725";
    return $.post("http://www.cmeonline.org/api/bookmarks", JSON.stringify(token), function(response) {
      var bookmark, _i, _len, _ref, _results;

      _ref = JSON.parse(response);
      _results = [];
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        bookmark = _ref[_i];
        _results.push(test_if_mine(bookmark));
      }
      return _results;
    });
  };

  hasBookmark = false;

  test_if_mine = function(bookmark) {
    if (bookmark.articleid === getParameterByName("id")) {
      hasBookmark = true;
      $('.bookmark-icon').attr('src', 'images/small/btn_sm_flag_active.png');
      return console.log("this article has a bookmark");
    }
  };

  insertBookmark = function() {
    var bookmark;

    bookmark = {};
    bookmark.articleid = articleid;
    bookmark.extra = "stub for now";
    bookmark.title = "test";
    bookmark.chapter = "1";
    bookmark.section = "1";
    bookmark.page = "1";
    bookmark.auth_token = "79d16d053261278cb9a6837f4ff8499c6ff17725";
    return $.post("http://www.cmeonline.org/api/bookmarks/insert", JSON.stringify(bookmark), function(response) {
      return console.log(response);
    });
  };

  makeFancyBox = function() {
    return $('.article-content img').fancybox();
  };

  scrollToAnchor = function(aid) {
    return window.location.url = "#" + aid;
  };

  $(document).ready(function() {
    var populateArticle;

    isBookmarked();
    $.post("http://www.cmeonline.org/api/articles/" + (getParameterByName('id')), JSON.stringify({
      "auth_token": "79d16d053261278cb9a6837f4ff8499c6ff17725"
    }), function(data) {
      return populateArticle(JSON.parse(data));
    });
    populateArticle = function(article) {
      if (article[0].id) {
        articleid = article[0].id;
      }
      if (article[0].title) {
        $('.article-title').text(article[0].title);
      }
      if (article[0].subtitle) {
        $('.article-subtitle').text(article[0].subtitle);
      }
      if (article[0].content) {
        $('.article-copy').html(article[0].content);
      }
      return makeFancyBox();
    };
    $('.searchhighlight').keyup(function(e) {
      unhighlight_words();
      if (e.keyCode === 13) {
        return highlight_words($(".searchhighlight").val());
      }
    });
    $('.article-btn-icon').not(".bookmark-icon").hover(function() {
      return $(this).attr('src', $(this).attr('active'));
    }, function() {
      return $(this).attr('src', $(this).attr('inactive'));
    });
    $('.bookmark-icon').hover(function() {
      return $(this).attr('src', $(this).attr('active'));
    }, function() {
      if (!hasBookmark) {
        return $(this).attr('src', $(this).attr('inactive'));
      }
    });
    $('.top-back-btn').click(function() {
      window.location.href = "articles.html";
      return $('.article-btn-icon').hover(function() {
        return $(this).attr('src', $(this).attr('active'));
      }, function() {
        return $(this).attr('src', $(this).attr('inactive'));
      });
    });
    $('.font-change-icon').click(function() {
      if (sizeUpCount % 4 === 0) {
        $('.article-content').css('font-size', '110%');
      } else if (sizeUpCount % 4 === 1) {
        $('.article-content').css('font-size', '125%');
      } else if (sizeUpCount % 4 === 2) {
        $('.article-content').css('font-size', '160%');
      } else {
        $('.article-content').css('font-size', '100%');
      }
      return sizeUpCount += 1;
    });
    $('.bookmark-icon').click(function() {
      insertBookmark();
      return false;
    });
    $('.text-search').click(function() {
      return $('.ui-input-search').toggle();
    });
    $('.toc-dropdown-btn').click(function() {
      return $('.toc, .article-content').toggle();
    });
    return $('.toc-section a').click(function() {
      $('.toc, .article-content').toggle();
      return scrollToAnchor($(this).attr("name"));
    });
  });

}).call(this);
