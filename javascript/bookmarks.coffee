createBookmarksList = (bookmarks)->
  createBookmarkBtn(bookmark) for bookmark in bookmarks

createBookmarkBtn = (bookmark)->
  $('.bookmarks-list .ui-listview').append "<li data-article-id='#{bookmark.articleid}' data-chapter='#{bookmark.section}' data-section='#{bookmark.section}' data-page='#{bookmark.page}'  class='ui-li ui-li-static ui-btn-up-c ui-li-has-thumb ui-first-child ui-last-child'><img src='images/small/icon_02_bookmarks.png' class='article-icon ui-li-thumb' data-bookmark-id='#{bookmark.id}'  /><div class='bookmarks-listitem-center' ><h3 >#{bookmark.title}</h3><p class='ui-li-desc'>Chapter #{bookmark.chapter}, Section #{bookmark.section}, Page #{bookmark.page}</p></div><img class='right-arrow-icon' src='images/small/right-arrow.png' /></li>"
  $('.bookmarks-list ul li').click ->
    window.location.href = "article.html?id=#{$(this).attr('data-article-id')}" 

getParameterByName = (name)->
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]")
    regexS = "[\\?&]" + name + "=([^&#]*)"
    regex = new RegExp(regexS)
    results = regex.exec window.location.search
    if results == null
        ""
    else
        decodeURIComponent results[1].replace(/\+/g, " ")


$(document).ready ->
  $.post 'http://www.cmeonline.org/api/bookmarks', JSON.stringify({ "auth_token" : "79d16d053261278cb9a6837f4ff8499c6ff17725"}), (data)-> createBookmarksList(JSON.parse(data))
  

	

