getParameterByName = (name)->
  name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]")
  regexS = "[\\?&]" + name + "=([^&#]*)"
  regex = new RegExp(regexS)
  results = regex.exec window.location.search
  if results == null
      ""
  else
      decodeURIComponent results[1].replace(/\+/g, " ")

createArticlesList = (articles)->
  createArticleBtn(article) for article in articles
  $('.article-list ul li').click ->
    window.location.href = "article.html?id=#{$(this).attr('data-article-id')}"


createArticleBtn = (article)->
  $('.article-list .ui-listview').append "<li data-article-id='#{article.id}' class='ui-li ui-li-static ui-btn-up-c ui-li-has-thumb ui-first-child ui-last-child'><img src='#{article.image}' class='article-icon ui-li-thumb' /><h3 class='ui-li-heading'>#{article.title}</h3><p class='ui-li-desc'>Subtitle description of article that summarizes content</p></li>"

createQuizzesList = (quizzes)->
  createQuizBtn(quiz) for quiz in quizzes
  $('.quizzes-list ul li').click ->
      window.location.href = "quiz.html?id=#{$(this).attr('data-quiz-id')}"


createQuizBtn = (quiz)->
  $('.quizzes-list .ui-listview').append "<li data-quiz-id='#{quiz.id}'  class='ui-li ui-li-static ui-btn-up-c ui-li-has-thumb ui-first-child ui-last-child'><img class='article-icon ui-li-thumb'  src='#{quiz.image}' /><h3 class='ui-li-heading'>#{quiz.title}</h3><p class='ui-li-desc'>#{quiz.number_questions} Questions<br/>#{quiz.credits}</p></li>"

$(document).ready ->
  $.post 'http://www.cmeonline.org/api/articles', JSON.stringify({ "auth_token" : "79d16d053261278cb9a6837f4ff8499c6ff17725"}), (data)-> createArticlesList(JSON.parse(data))
  $.post 'http://www.cmeonline.org/api/test', JSON.stringify({ "auth_token" : "79d16d053261278cb9a6837f4ff8499c6ff17725"}), (data)-> createQuizzesList(JSON.parse(data))

  $('.articles-tab').click ->
    $('.quizzes-list').hide()
    $('.article-list').show()
    $('.articles-active').show()
    $('.quizzes-active').hide()
    false

  $('.quizzes-tab').click ->
    $('.article-list').hide()
    $('.quizzes-list').show()
    $('.quizzes-active').show()
    $('.articles-active').hide()
    false

  $('.article-list ul li').click ->
    window.location.href = "article.html?id=#{$(this).attr('data-article-id')}"

  $('.quizzes-list ul li').click ->
    window.location.href = "quiz.html?id=#{$(this.attr('data-quiz-id'))}"

  if getParameterByName("selected") == "quizzes"
    $('.article-list').hide()
    $('.quizzes-list').show()
    $('.quizzes-active').show()
    $('.articles-active').hide()
    
