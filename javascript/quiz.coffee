getParameterByName = (name)->
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]")
    regexS = "[\\?&]" + name + "=([^&#]*)"
    regex = new RegExp(regexS)
    results = regex.exec window.location.search
    if results == null
        ""
    else
        decodeURIComponent results[1].replace(/\+/g, " ")

questions = []
questionIndices = []
resultIndex = 1
result = {}
index = 0
length = 0
testid = {}
time_started = (new Date()).getTime()
submissionData = {}

createTest = (data)->
   questions = JSON.parse(data)
   length = questions.length
   $('.quiz-title').text("#{questions[index].title}")
   testid = questions[0].testid
  
createQuestion = ->
   $('.quiz-subtitle').text("Question #{index+1}")
   $('.quiz-copy').text(questions[index].question)
  
   if questions[index].a1 then $('.a1').html("#{questions[index].a1}") else $('.a1').hide()
   if questions[index].a2 then $('.a2').html("#{questions[index].a2}") else $('.a2').hide()
   if questions[index].a3 then $('.a3').html("#{questions[index].a3}") else $('.a3').hide()
   if questions[index].a4 then $('.a4').html("#{questions[index].a4}") else $('.a4').hide()
   if questions[index].a5 then $('.a5').html("#{questions[index].a5}") else $('.a5').hide()

        
         
submitCompletedQuiz = ->
  submissionData.auth_token = "79d16d053261278cb9a6837f4ff8499c6ff17725"
  submissionData.time_taken = ((new Date()).getTime() - time_started) / 1000.0
  submissionData.id = testid
  #(submissionData[qIndex + 1] = answers[qIndex]) for qIndex in [0..length]
  $.post "http://www.cmeonline.org/api/test/grade", JSON.stringify(submissionData), (response)->
        displayResults(response)
        
displayResults = (results)->
        $('.quiz-content').hide()
        $('.quizresults').show()
        results = JSON.parse(results)
        console.log(results)
        questionCount = 1
        displaySingleResult(results[questionIndex]) for questionIndex in questionIndices       
        $('.score').append " #{results.score}%"
        $('.quizresults').append "<div class='quizsummary'><p style='display: block'>You had #{results.correct} correct and #{results.incorrect} incorrect out of #{results.totalq} questions for a total score of #{results.score}%</p></div>"
        if results.result != "FAIL"
                $('.quizresults').append "<img src='images/getcredit.jpg' class='getcredit' />"
                $('.getcredit').click ->
                        $('.quizresults').hide()
                        $('.quizsurvey').show()
        else
                $('.quizsummary').append "<p>Sorry, your score was not high enough to pass. If you would like to try again, please press the back button.</p>"

displaySingleResult = (result) ->
        console.log(result)
        if result.answerstatus == "Correct"
          color = "green"
          image = "images/correct_check.png"
        else
          image = "images/wrong_answer_box.png"
          color = "red"
        
        $('.quizresults').append "<div class='result' style='display:block;'>
            <div class='correct' style='float: left;'>
              <img src='#{image}' />
            </div>
            <div class='number'>
              <h1>#{resultIndex}. </h1>
            </div>
             <div class='resultcopy'>
              <p style='color: #{color}'>#{result.question}</p>
              <p class='answercopy'>Answer: #{result.answer}</p>
            </div>
          </div>"
        resultIndex = resultIndex + 1


submitAnswer = (answerIndex, questionIndex)->
  if answerIndex
    submissionData[questions[questionIndex].id] = answerIndex    
    questionIndices.push questions[questionIndex].id
 
moveNext = ->
  index = index + 1
  createQuestion()
                 
$(document).ready ->   
  $.post "http://www.cmeonline.org/api/test/#{getParameterByName('id')}", JSON.stringify({ "auth_token" : "79d16d053261278cb9a6837f4ff8499c6ff17725"}), (response)->
        createTest(response)
        createQuestion() # create first question
        
  $('.quiz-answer-li').click ->
       if index < length - 1  #last question was entered
          submitAnswer($(this).attr('data-answer-index'), index)
          moveNext()
       else
         submitAnswer($(this).attr('data-answer-index'), index)
         submitCompletedQuiz()

   $('.getcreditfinal').click ->
        formdata = $("#myform").jsonify()
        formdata.auth_token = "79d16d053261278cb9a6837f4ff8499c6ff17725"
        formdata.origid = getParameterByName('id')
        $.post 'http://www.cmeonline.org/api/posttest/submit', JSON.stringify(formdata), (response) ->
                console.log(response)
                downloadurl =  "http://www.cmeonline.org/#{JSON.parse(response)[0].status}&auth_token=79d16d053261278cb9a6837f4ff8499c6ff17725"
                if JSON.parse(response)[0].status
                        $('#downloadpdf').attr('href', downloadurl)
                        $('.quizsurvey').hide()
                        $('.downloadcertificate').show()
               
                        
                        
                