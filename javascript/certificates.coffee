getParameterByName = (name)->
	    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]")
	    regexS = "[\\?&]" + name + "=([^&#]*)"
	    regex = new RegExp(regexS)
	    results = regex.exec window.location.search
	    if results == null
	        ""
	    else
	        decodeURIComponent results[1].replace(/\+/g, " ")

createCertificatesList = (certificates)->
    createCertificateBtn(certificate) for certificate in certificates
    $('.certificates-list ul li').click -> 
        window.location.href = "http://www.cmeonline.org/#{$(this).attr('data-url')}&auth_token=79d16d053261278cb9a6837f4ff8499c6ff17725"
       

createCertificateBtn = (certificate)->
    if certificate.url
        $('.certificates-list .ui-listview').append "<li data-url='#{certificate.url}'  class='ui-li ui-li-static ui-btn-up-c ui-li-has-thumb ui-first-child ui-last-child'><img class='ui-li-thumb' src='images/small/icon_starburst.png' /><div class='certificate-listitem-center'><h3 class='ui-li-heading'>#{certificate.title}</h3><p class='ui-li-desc'>Score: #{certificate.score}</p><p class='ui-li-desc'>Date: #{certificate.date}</p></div><img class='pdf-dl-icon' src='images/small/icon_pdf.png' /></li>"
    

$(document).ready ->
    $.post 'http://www.cmeonline.org/api/certificates', JSON.stringify({ "auth_token" : "79d16d053261278cb9a6837f4ff8499c6ff17725"}), (data)-> createCertificatesList(JSON.parse(data))

    $('.certificates-list ul li').click ->
        window.location.href = "http://www.cmeonline.org/#{$(this).attr('data-url')}&auth_token=79d16d053261278cb9a6837f4ff8499c6ff17725"
   
