$(document).ready ->
  $('.register-tab').click ->
    $('.sign-in').hide()
    $('.register').show()
    false

  $('.signin-tab').click ->
    $('.register').hide()
    $('.sign-in').show()
    false

  $('.signin-btn').click ->
    window.location.href = "index.html"
    false

  $('.register-btn').click ->
    $('.register').hide()
    $('.sign-in').show()
    false

  getParameterByName = (name)->
    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]")
    regexS = "[\\?&]" + name + "=([^&#]*)"
    regex = new RegExp(regexS)
    results = regex.exec window.location.search
    if results == null
        ""
    else
        decodeURIComponent results[1].replace(/\+/g, " ")