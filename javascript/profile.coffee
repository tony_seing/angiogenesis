user_profile = {}

getParameterByName = (name)->
	    name = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]")
	    regexS = "[\\?&]" + name + "=([^&#]*)"
	    regex = new RegExp(regexS)
	    results = regex.exec window.location.search
	    if results == null
	        ""
	    else
	        decodeURIComponent results[1].replace(/\+/g, " ")

fillProfileInfo = (profile)->
    user_profile = profile
    $('.email').val(profile.email)
    $('.zip').val(profile.postal)
    $('.institution').val(profile.company)

submissionHandler = (data)->
    console.log(data)

submitProfile = ->
    user_profile.email = $('.email').val()
    user_profile.postal = $('.zip').val()
    user_profile.company = $('.institution').val()
    user_profile.auth_token = "79d16d053261278cb9a6837f4ff8499c6ff17725"
    $.post 'http://www.cmeonline.org/api/user/update', JSON.stringify(user_profile), (data)-> submissionHandler(data)


$(document).ready ->
    $.post 'http://www.cmeonline.org/api/user/info', JSON.stringify({ "auth_token" : "79d16d053261278cb9a6837f4ff8499c6ff17725"}), (data)-> fillProfileInfo(JSON.parse(data))
    $('.submit-btn').click ->
        submitProfile()
        window.location.href = "index.html"
